from django.urls import path
from todos.views import to_do_list, todo_list_detail, todo_list_create

urlpatterns = [
    path("", to_do_list, name="to_do_list"),
    path("<int:id>/", todo_list_detail, name="todo_list_detail"),
    path("create/", todo_list_create, name="todo_list_create"),
]
